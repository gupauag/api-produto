package br.com.lead.collector.LeadCollector.controllers;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import br.com.lead.collector.LeadCollector.models.Lead;
import br.com.lead.collector.LeadCollector.models.Produto;
import br.com.lead.collector.LeadCollector.security.JWTUtil;
import br.com.lead.collector.LeadCollector.services.LeadService;
import br.com.lead.collector.LeadCollector.services.LoginUsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    private LeadService leadService;

    @MockBean
    private JWTUtil jwtUtil;
    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @Autowired
    private MockMvc mockMvc;

    private Lead lead;
    private Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNomeMaluco("Gustavo");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("gustavo@teste.com");
        lead.setId(1);
        //lead.setData(LocalDate.now());

        produto = new Produto();
        produto.setDescricao("Cafe do bom");
        produto.setIdProduto(1);
        produto.setNome("Café");
        produto.setPreco(20.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);

    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });
        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads") //rota que é chamada
                .contentType(MediaType.APPLICATION_JSON) //tipo de entrada da API
                .content(jsonDeLead)) //entrada da api
                .andExpect(MockMvcResultMatchers.status().isCreated()) //valida o status de retorno
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1))) // campos que eu quero validar - valida se o retorno é 1, o q foi setado no lambida acima
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())));
    }
    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarRegistrarLeadNaoValido() throws Exception {
        lead = new Lead();
        lead.setNomeMaluco("Gustavo");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());


        //Para validar o retorno de um DTO por exemplo
//        ResultActions resultActions =  mockMvc.perform(MockMvcRequestBuilders.post("/leads")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(jsonDeLead))
//                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
//        MvcResult mvcResult = resultActions.andReturn();
//        String jsonResposta = mvcResult.getResponse().getContentAsString();
//        LeadDTO leadDTO = mapper.readValues(jsonResposta, LeaDTO.class);
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarExibirTodosSemParametro() throws Exception {

        Mockito.when(leadService.buscarTodos()).then(leadObjeto -> {
            List<Lead> leads = new ArrayList<>();
            leads.add(lead);
            leads.add(lead);
            return leads;
        });

        //ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        //String jsonDeLead = mapper.writeValueAsString(lead);

        //espera um retorno de dois registros
        mockMvc.perform(MockMvcRequestBuilders.get("/leads"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));

    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarExibirTodosTiposEnums() throws Exception {

        Mockito.when(leadService.buscaTodosPorTipoLead(Mockito.any(TipoLeadEnum.class))).then(leadObjeto -> {
            List<Lead> leads = new ArrayList<>();
            leads.add(lead);
            leads.add(lead);
            return leads;
        });

        //espera um retorno de dois registros
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .param("tipoDeLead", "QUENTE"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarBuscarPorId() throws Exception {

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).then(leadObjeto -> {
            lead.setId(1);
            return lead;
        });

        //espera um retorno de 1 registro
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarBuscarPorIdInesitente() throws Exception {

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).then(leadObjeto -> {
            throw new RuntimeException();
        });

        //espera um retorno de 1 registro
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarAtualizarLeadExistente() throws Exception {

        Mockito.when(leadService.atualizarLead(Mockito.anyInt(), Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeLead = mapper.writeValueAsString(lead);

        //espera um retorno de 1 registro
        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON) //tipo de entrada da API
                .content(jsonDeLead)) //entrada da api
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarAtualizarLeadInexistente() throws Exception {

        Mockito.when(leadService.atualizarLead(Mockito.anyInt(), Mockito.any(Lead.class))).then(leadObjeto -> {
            throw new RuntimeException();
        });

        ObjectMapper mapper = new ObjectMapper(); //Conversor de Classe em Json!
        String jsonDeLead = mapper.writeValueAsString(lead);

        //espera um retorno de 1 registro
        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON) //tipo de entrada da API
                .content(jsonDeLead)) //entrada da api
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarDeletarLeadExistente() throws Exception {
        //Mockito.when(leadService.deletarLead(Mockito.anyInt())).then();

        //Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.anyInt());

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

        //Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.anyInt());
    }
    @Test
    @WithMockUser(username = "gustavo2@teste.com", password = "teste1234")
    public void testarDeletarLeadInexistente() throws Exception {

        Mockito.doThrow(new RuntimeException("ERRO")).when(leadService).deletarLead(Mockito.anyInt());

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        //Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.anyInt());
    }

}
