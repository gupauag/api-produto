package br.com.lead.collector.LeadCollector.services;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import br.com.lead.collector.LeadCollector.models.Lead;
import br.com.lead.collector.LeadCollector.models.Produto;
import br.com.lead.collector.LeadCollector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNomeMaluco("Gustavo");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("gustavo@teste.com");

        produto = new Produto();
        produto.setDescricao("Cafe do bom");
        produto.setIdProduto(1);
        produto.setNome("Café");
        produto.setPreco(20.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }


    @Test
    public void testarSalvarLead(){
        //List<Long> idProdutos = Arrays.asList(1L);
        //Mockito.when(produtoService.buscarPorTodosOsId(idProdutos)).thenReturn(produtos);
        // anyList() faz com q qualquer lista seja reconhecida
        Mockito.when(produtoService.buscarPorTodosOsId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNomeMaluco("Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("teste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        //Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTeste);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(leadLamb -> {
            leadTeste.setId(1);
            return leadTeste;
        });

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto,leadObjeto.getProdutos().get(0));
    }


    @Test
    public void testarBuscarPorTodosOsLeads(){
        Iterable<Lead> leads = Arrays.asList(lead); //moca o leads com os valores que estão no objeto lead
        Mockito.when(leadRepository.findAll()).thenReturn(leads); //Mocka o método findAll do leadRepository com os dados do array leads

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads,leadIterable);
    }

    @Test
    public void testarBuscaTodosPorTipoLead(){
        Iterable<Lead> leads = Arrays.asList(lead); //moca o leads com os valores que estão no objeto lead
        Mockito.when(leadRepository.findAllByTipoLead(lead.getTipoLead())).thenReturn(leads); //Mocka o método findAll do leadRepository com os dados do array leads

        Iterable<Lead> leadIterable = leadService.buscaTodosPorTipoLead(TipoLeadEnum.QUENTE);

        Assertions.assertEquals(lead, leadIterable);

    }

    @Test
    public void testarBuscarPorId(){
        lead.setId(1);
        Optional<Lead> leadOptional = Optional.of(lead);//moca o leads com os valores que estão no objeto lead
        Mockito.when(leadRepository.findById(1)).thenReturn(leadOptional); //Mocka o método findAll do leadRepository com os dados do array leads

        Optional<Lead> LeadObjeto = Optional.of(leadService.buscarPorId(1));


        Assertions.assertEquals(LeadObjeto, leadOptional);
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    leadService.buscarPorId(2);
                });
    }

    @Test
    public void testarAtualizarLead(){

        //Mockito.when(produtoService.buscarPorTodosOsId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setId(1);
        leadTeste.setNomeMaluco("Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("teste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTeste);
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true); //Mocka o método findAll do leadRepository com os dados do array leads

        Lead leadObjeto = leadService.atualizarLead(1,leadTeste);

        Assertions.assertEquals(leadObjeto.getId(), 1);

    }

    @Test
    public void testarAtualizarLeadNaoencontrado() {
        Lead leadTeste = new Lead();
        leadTeste.setId(1);
        leadTeste.setNomeMaluco("Teste");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("teste@teste.com");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    leadService.atualizarLead(2, leadTeste);
                });
    }

    @Test
    public void testarDeletarLead(){

        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        leadService.deletarLead(800);

        //verifica se o metodo delete foi chamado 1 vez ou mais.
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }




}
