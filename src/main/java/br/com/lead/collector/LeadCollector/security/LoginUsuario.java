package br.com.lead.collector.LeadCollector.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUsuario implements UserDetails {

    private int id;
    private String email;
    private String senha;

    public LoginUsuario() {
    }

    public LoginUsuario(int id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return email;
    }

    //Tempo para expirar a senha
    @Override
    public boolean isAccountNonExpired() {
        return true; //true ou false
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //valida se a conta esta bloqueada
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
