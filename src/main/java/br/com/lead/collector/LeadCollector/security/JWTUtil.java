package br.com.lead.collector.LeadCollector.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component //será usado como componentes de outras classes. Não precisa mais instanciar a classe por exemplo
public class JWTUtil {

    @Value("${jwt.segredo}")
    private String segredo;

    @Value("${jwt.expiracao}")
    private Long expiracao;

    public String gerarToken(String username){
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis()+expiracao))
                .signWith(SignatureAlgorithm.HS512, segredo.getBytes())
                .compact(); // responsavel por fazer a criptografia
        return token;
    }

    //Configuração de reconhecimento de token

    public boolean tokenValido(String token){
        try {
            Claims claims = getClaims(token);  //monta o token para ser utilizado as partes separadamente e assim compará-lo

            String email = claims.getSubject();

            Date dataDeExpiracao = claims.getExpiration();
            Date dataAtual = new Date(System.currentTimeMillis());

            if(email != null && dataDeExpiracao != null && dataAtual.before(dataDeExpiracao)){
                return true;
            } else return false;

        }catch (Exception e){
            return false;
        }
    }

    public Claims getClaims(String token){
        try{
            return Jwts.parser().setSigningKey(segredo.getBytes()).parseClaimsJws(token).getBody();
        }catch (Exception e){
            throw new RuntimeException();
        }
    }

    public String getUserName(String token){
        Claims claims = getClaims(token);
        String username = claims.getSubject();
        return username;
    }

}
