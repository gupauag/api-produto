package br.com.lead.collector.LeadCollector.security;

import br.com.lead.collector.LeadCollector.models.DTO.CredenciaisDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroDeAutenticacao extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private JWTUtil jwtUtil;

    public FiltroDeAutenticacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }


    //Método para autenticação
    //Verifica se o usuario esta na base
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            //ta convertendo o json de entrada para uma classe via request
            CredenciaisDTO credenciaisDTO = objectMapper.readValue(request.getInputStream(), CredenciaisDTO.class);

            //passar para essa clase os dados do DTO para receber um token
            UsernamePasswordAuthenticationToken userNameToken = new UsernamePasswordAuthenticationToken(
                    credenciaisDTO.getEmail(), credenciaisDTO.getSenha(), new ArrayList<>());

            //COnfirmo a autenticação para retorno dela
            Authentication authentication = authenticationManager.authenticate(userNameToken);

            return authentication;
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    //metodo para responder a autenticação do usuario
    // caso o usuario esteja positivo, esse metodo q retorna o token criado
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                 FilterChain chain, Authentication authResult)
                    throws AuthenticationException, IOException, ServletException {

        String username = ((LoginUsuario) authResult.getPrincipal()).getUsername();
        String token = jwtUtil.gerarToken(username);
        response.addHeader("Autorization", "Bearer "+token); //boa pratica responder o token pelo header e o front pega do header o token

        //super.successfulAuthentication(request,response,chain,authResult);

    }
}
