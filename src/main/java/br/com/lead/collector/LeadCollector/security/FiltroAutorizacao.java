package br.com.lead.collector.LeadCollector.security;

import br.com.lead.collector.LeadCollector.services.LoginUsuarioService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;

    private LoginUsuarioService loginUsuarioService;

    public FiltroAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
                             LoginUsuarioService loginUsuarioService) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.loginUsuarioService = loginUsuarioService;
    }

    //autorização sera feita por esse metodo
    //Verifica se mandou o token

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            String tokenLimpo = authorizationHeader.substring(7); //retira os primeiros 7 caracteres com o substring;

            // cria a partir do token, a autorização do usuario
            UsernamePasswordAuthenticationToken autenticacao = getAutenticaco(request, tokenLimpo);

            if(autenticacao != null){
                //cola as permissoes do usuario no contexto de segurança do spring
                SecurityContextHolder.getContext().setAuthentication(autenticacao);
            }
        }
        //devolve pro ConfiguracaoDeSegurana (Securitie) as permissoes de request/response
        chain.doFilter(request, response);

    }

    private UsernamePasswordAuthenticationToken getAutenticaco(HttpServletRequest request, String token){
        if(jwtUtil.tokenValido(token)){
            String username = jwtUtil.getUserName(token);
            UserDetails usuario = loginUsuarioService.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
        }
        return null;
    }

}
