package br.com.lead.collector.LeadCollector.repositories;

import br.com.lead.collector.LeadCollector.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByEmail(String email);

}
