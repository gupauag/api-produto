package br.com.lead.collector.LeadCollector.services;

import br.com.lead.collector.LeadCollector.models.Usuario;
import br.com.lead.collector.LeadCollector.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Usuario salvarUsuario(Usuario usuario){
        String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());
        usuario.setSenha(encoder);
        Usuario usuarioObjeto = usuarioRepository.save(usuario);

       return usuarioObjeto;
    }


}
