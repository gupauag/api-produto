package br.com.lead.collector.LeadCollector.services;

import br.com.lead.collector.LeadCollector.models.Lead;
import br.com.lead.collector.LeadCollector.models.Produto;
import br.com.lead.collector.LeadCollector.repositories.ProdutosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutosRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjetc = produtoRepository.save(produto);
        return produto;
    }

    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Iterable<Produto> buscaTodosPorNome(String nome){
        Iterable<Produto> produtos = produtoRepository.findAllByNome(nome);
        return produtos;
    }
    public Iterable<Produto> buscaTodosPorAlgumaDescricao(String nome){
        Iterable<Produto> produtos = produtoRepository.findByDescricaoContains(nome);
        return produtos;
    }

    public Iterable<Produto> buscaPorNomeEPreco(String nome, double preco){
        //variavel Optional pode ou não ter um Lead dentro dela!
        Iterable<Produto> optionalProduto = produtoRepository.findByNomeAndPreco(nome, preco);
        if(optionalProduto.equals(null)) throw new RuntimeException("O Produto não foi encontrado");
        return optionalProduto;
    }

    public Produto buscarPorId(Long id){
        //variavel Optional pode ou não ter um Lead dentro dela!
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if(optionalProduto.isPresent()) // metodo que verifica se tem ou não porduto na consulta dentro do Optional
            return optionalProduto.get();
        throw new RuntimeException("O Produto não foi encontrado");
    }

    public Produto atualizarProduto(Long id, Produto produto){
        if(produtoRepository.existsById(id)){
            produto.setIdProduto(id);
            Produto produtoObjeto = salvarProduto(produto);

            return produtoObjeto;
        }
        throw new RuntimeException("O Produto não foi encontrado");
    }

    public void deletarProduto(Long id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        }else
            throw new RuntimeException("O Produto não foi encontrado");
    }

    public List<Produto> buscarPorTodosOsId(List<Long> id){
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List) produto;
    }

}
