package br.com.lead.collector.LeadCollector.configurations;

import br.com.lead.collector.LeadCollector.security.FiltroAutorizacao;
import br.com.lead.collector.LeadCollector.security.FiltroDeAutenticacao;
import br.com.lead.collector.LeadCollector.security.JWTUtil;
import br.com.lead.collector.LeadCollector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


//Principal classe de segurança
// toda chamada da minha api passa por essa classe

@Configuration
@EnableWebSecurity //definie a classe ativa de segurança do spring e não as configuraçoes padroes
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private LoginUsuarioService loginUsuarioService;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/leads",
            "/leads/*",
            "/produtos"
    };
    private static final String[] PUBLIC_MATCHERS_POST = {
            "/leads",
            "/usuario",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable(); //desabilitando o token do formulario, permitindo qlqer um fazer requisição
        http.cors();

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .anyRequest().authenticated();

        //isso reduz consumo de memória
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); //stateless pq não utilizamos sessão na api

        //Filtros JWT
        //Se for pra fazer autenticação ele direciona para essa rota
        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));

        // Se for fazer alguma coisa na api ele entra no fluxo de autorização
        //Uma requisição feita as rotas, entra aqui
        http.addFilter(new FiltroAutorizacao(authenticationManager(), jwtUtil, loginUsuarioService));
        //caso não esteja permitido, o sistema retorna o erro!

    }

    //trava a origem de consumo!
    //@CrossOrigin(origins = "http://123.78.0.00", value = "/leads")
    @Bean
    CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
                                    //todos os endpoints podem receber requisições defaults em minha API
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());//libera os acessos de outras fontes externas para todas as minhas URLS
        // trava a origem de outra forma
        //cors.registerCorsConfiguration("/leads", new CorsConfiguration().checkOrigin("http://127.0.0.0"));
        return cors;
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
