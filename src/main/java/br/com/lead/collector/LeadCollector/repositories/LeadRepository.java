package br.com.lead.collector.LeadCollector.repositories;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import br.com.lead.collector.LeadCollector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead,Integer> {

    //@Query("Select * from lead where lead.tipolead == ?") //posso mandar a query para o metodo ou usar o hibernate puto
    Iterable<Lead> findAllByTipoLead(TipoLeadEnum leadEnum); // nesse caso o proprio hibernate já monta a query por conta do findAllBy+Campo
}
