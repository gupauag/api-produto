package br.com.lead.collector.LeadCollector.controllers;

import br.com.lead.collector.LeadCollector.models.Produto;
import br.com.lead.collector.LeadCollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto salvarProduto(@RequestBody Produto produto) {

        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    //required = false significa q não eh obrigatorio
    public Iterable<Produto> exibirTodos(@RequestParam(name = "nome",required = false) String nome,
                                         @RequestParam(name = "descricao",required = false) String descricao,
                                         @RequestParam(name = "preco", required = false) Double preco ){
        Iterable<Produto> produto;
        if(nome != null) produto = produtoService.buscaTodosPorNome(nome);
        if(descricao!=null) produto = produtoService.buscaTodosPorAlgumaDescricao(descricao);
        if(nome!=null && preco!=null) produto = produtoService.buscaPorNomeEPreco(nome,preco);
        else produto = produtoService.buscarTodos();
        return produto;
    }

    //http://localhost:8080/1
    //@GetMapping("/{id}/{outravarivavel}")
    @GetMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    public Produto buscarPorId(@PathVariable(name = "id") long id){
        try {
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    public Produto atualizarProduto(@PathVariable(name = "id") long id, @RequestBody Produto produto){
        try {
            Produto produtoObjeto = produtoService.atualizarProduto(id,produto);
            return produtoObjeto;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    public ResponseEntity<?> deletarProduto(@PathVariable(name = "id") long id){
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
