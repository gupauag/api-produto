package br.com.lead.collector.LeadCollector.enums;

public enum TipoLeadEnum {
    QUENTE,
    FRIO,
    ORGANICO;
}
