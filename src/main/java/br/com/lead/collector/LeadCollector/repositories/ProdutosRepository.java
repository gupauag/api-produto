package br.com.lead.collector.LeadCollector.repositories;

import br.com.lead.collector.LeadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProdutosRepository extends CrudRepository<Produto,Long> {

    Iterable<Produto> findAllByNome(String nome);

    Iterable<Produto> findByDescricaoContains(String nome);

    Iterable<Produto> findByNomeAndPreco(String nome, double preco);


}
