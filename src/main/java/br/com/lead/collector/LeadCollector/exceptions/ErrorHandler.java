package br.com.lead.collector.LeadCollector.exceptions;

import br.com.lead.collector.LeadCollector.exceptions.errors.MensagemDeErro;
import br.com.lead.collector.LeadCollector.exceptions.errors.ObjetoDeErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.naming.Binding;
import java.net.BindException;
import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @Value("${mensagem.padrao.de.validacao")
    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class) // erro disparado na validação dos argumentos passados
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY) // informa que não foi processado por conta das entidades
    @ResponseBody //indica que a resposta será no body da api
    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {

        HashMap<String, ObjetoDeErro> erros = new HashMap<>();

        BindingResult resultado = exception.getBindingResult();

        for (FieldError erro : resultado.getFieldErrors()) {
            erros.put(erro.getField(), new ObjetoDeErro(erro.getDefaultMessage(), erro.getRejectedValue().toString()));
        }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
               this.mensagemPadrao, erros);

        return mensagemDeErro;
    }
}
