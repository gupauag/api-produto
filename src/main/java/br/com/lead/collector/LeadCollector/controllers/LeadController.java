package br.com.lead.collector.LeadCollector.controllers;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import br.com.lead.collector.LeadCollector.models.Lead;
import br.com.lead.collector.LeadCollector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping                         //@Valid usa a validação incluida na model! Valida os campos com as regras incluidas na model
    public ResponseEntity<Lead> registrarLead(@RequestBody @Valid Lead lead){
        Lead leadObjeto = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(leadObjeto); // retorno no body e com o status 201
    }

//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED) // muda o return code para 201 - criado
                                            //@Valid usa a validação incluida na model! Valida os campos com as regras incluidas na model
//    public Lead registrarLead(@RequestBody @Valid Lead lead){
//        return leadService.salvarLead(lead);
//    }

    @GetMapping                                                       //required = false significa q não eh obrigatorio
    public Iterable<Lead> exibirTodos(@RequestParam(name = "tipoDeLead",required = false) TipoLeadEnum tipoLeadEnum){
        Iterable<Lead> leads;
        if(tipoLeadEnum != null) leads = leadService.buscaTodosPorTipoLead(tipoLeadEnum);
        else leads = leadService.buscarTodos();
        return leads;
    }

    //http://localhost:8080/1
    //@GetMapping("/{id}/{outravarivavel}")
    @GetMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    public Lead buscarPorId(@PathVariable(name = "id") int id){
        try {
            Lead lead = leadService.buscarPorId(id);
            return lead;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
   }

    @PutMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    public Lead atualizarLead(@PathVariable(name = "id") int id, @RequestBody Lead lead){
        try {
            Lead leadobjeto = leadService.atualizarLead(id,lead);
            return leadobjeto;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}") // desta forma a chamada não é mais por parametro e sim por parametro
    @ResponseStatus(HttpStatus.NO_CONTENT) // não encontrou
    public void deletarLead(@PathVariable(name = "id") int id){
        try {
            leadService.deletarLead(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

   //Não é uma boa pratica fazer desta forma - passar o q esta buscando na url
//    @GetMapping("/buscaTipoDoLead")
//    public Iterable<Lead> exibirTodos(@RequestBody TipoLeadEnum tipoLeadEnum){
//        Iterable<Lead> leads = leadService.buscarTodos();
//        return leads;
//    }

}
