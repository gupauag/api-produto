package br.com.lead.collector.LeadCollector.services;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import br.com.lead.collector.LeadCollector.models.Lead;
import br.com.lead.collector.LeadCollector.models.Produto;
import br.com.lead.collector.LeadCollector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoService produtoService;

    public Lead salvarLead(Lead lead){

        List<Long> idProdutos = new ArrayList<>();

        for(Produto produto:lead.getProdutos()){
            long id = produto.getIdProduto();
            idProdutos.add(id);
        }
        List<Produto> produtos = produtoService.buscarPorTodosOsId(idProdutos);
        lead.setProdutos(produtos);

        LocalDate data = LocalDate.now();
        lead.setData(data);

        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodos(){
        Iterable<Lead> leads = leadRepository.findAll();
        return  leads;
    }

    public Iterable<Lead> buscaTodosPorTipoLead(TipoLeadEnum leadEnum){
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(leadEnum);
        return leads;
    }

    public Lead buscarPorId(int id){
        //variavel Optional pode ou não ter um Lead dentro dela!
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if(optionalLead.isPresent()) // metodo que verifica se tem ou não lead na consulta dentro do Optional
            return optionalLead.get();
        throw new RuntimeException("O Lead não foi encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
        if(leadRepository.existsById(id)){
            lead.setId(id);
            Lead leadObjeto = salvarLead(lead);

            return leadObjeto;
        }
        throw new RuntimeException("O Lead não foi encontrado");
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        }else
            throw new RuntimeException("O Lead não foi encontrado");
    }

}
