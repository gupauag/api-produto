package br.com.lead.collector.LeadCollector.models;

import br.com.lead.collector.LeadCollector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties(value = {"data"}, allowGetters = true) //esse comando ignora valores enviado com a tag
                                                            // data, mas o get funcionará
@Table (name="leads")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Size(min=5, max = 100, message = "O nome deve ser entre 5 e 100 caracteres")
    private String nome;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "O email não pode ser null")
    private String email;

    //@Null(message = "O campo data não é preenchido pelo usuario")
    @JsonIgnoreProperties //esse comando ignora valores
    private LocalDate data;

    @NotNull(message = "O campo tipoLead não pode ser null")
    @Enumerated(EnumType.ORDINAL) // ORDINAL SIGNIFICA O NUMERO DA POSICAO DA STRING
    //@Enumerated(EnumType.STRING) // STRING SIGNIFICA O NOME CADASTRADO NO ENUM
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL) //para delete em cascata!
    //@JoinTable(name="leads_produtos", joinColumns = @JoinColumn(name="lead_id"),
      //      inverseJoinColumns = @JoinColumn(name="produto_id"))
    private List<Produto> produtos;


    public Lead() {
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNomeMaluco(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }
}
